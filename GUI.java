import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUI<T>	{
	/*Instance Variables*/
	private TicTacToe parent;
	private JFrame window = new JFrame("Tic-Tac-Toe");
	private JButton[] buttons = new JButton[9];
	private JButton undo = new JButton("Undo");
	private JButton redo = new JButton("Redo");
	private LinkedStack<Move> undos;
	private LinkedStack<Move> redos;

	public GUI(TicTacToe parent)	{
		undos = new LinkedStack();
		redos = new LinkedStack();
		
		undo.setEnabled(false);
		redo.setEnabled(false);
		
		this.parent = parent;
		/*Create Window*/
		window.setSize(300,300);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLayout(new GridLayout(4,3));

		ButtonListener listen = new ButtonListener();
	
		/*Add Buttons To The Window*/
		for(int i=0; i<=8; i++){
			buttons[i] = new JButton();
			window.add(buttons[i]);
			buttons[i].addActionListener(listen);
		}
		window.add(undo);
		window.add(redo);
		undo.addActionListener(listen);
		redo.addActionListener(listen);
	
		/*Make The Window Visible*/
		window.setVisible(true);
	}
	
	public void playAgainDialog() {		
		int response = JOptionPane.showConfirmDialog(null, "Do you want to play again?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		
		if(response == JOptionPane.YES_OPTION)   parent.reset();
		else                                     System.exit(0);
		
	}
	
	public void reset() {
		for(int i=0; i<=8; i++){
			buttons[i].setText("");
			buttons[i].setEnabled(true);
		}
		undos = new LinkedStack();
		redos = new LinkedStack();
	}

	public String[] getData()	{
		String[] data = new String[9];
		for(int i=0;i<9;i++)
			data[i] = buttons[i].getText();
		return data;
	}
	public void showMessage(String text)	{
		JOptionPane.showMessageDialog(null,text);
	}
	public void undo()	{
		JButton undoButton = undos.pop().getButton();
		redos.push(new Move(undoButton));
		undoButton.setText("");
		undoButton.setEnabled(true);
	}
	public void redo()	{
		JButton redoButton = redos.pop().getButton();
		undos.push(new Move(redoButton));
		redoButton.setText(parent.getLetter());
		redoButton.setEnabled(false);
	}
	public void updateButtons()	{
		undo.setEnabled(!undos.isEmpty());
		redo.setEnabled(!redos.isEmpty());
	}

	private class ButtonListener implements ActionListener	{
		/**
		 When an object is clicked, perform an action.
		 @param a action event object
		 */
		public void actionPerformed(ActionEvent a) {
			JButton pressedButton = (JButton)a.getSource();

			if(pressedButton == undo)
				parent.undo();
			else if(pressedButton == redo)
				parent.redo();
			else	{
				redos = new LinkedStack();
				undos.push(new Move(pressedButton));
				parent.setLetter();
				/*Write the letter to the button and deactivate it*/
				pressedButton.setText(parent.getLetter());
				pressedButton.setEnabled(false);
				parent.checkWin();
				parent.printWin();
				updateButtons();
			}
		}	
	}
}
