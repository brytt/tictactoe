public class TicTacToe	{
	private GUI gui;
	private int count, xWins, oWins = 0;
	private int[][] winCombinations = new int[][] {
							{0, 1, 2},	//horizontal wins
							{3, 4, 5},
							{6, 7, 8},

							{0, 3, 6},	//verticle wins
							{1, 4, 7},
							{2, 5, 8},

							{0, 4, 8},	//diagonal wins
							{2, 4, 6}	};
	private boolean win = false;
	private String letter = "";

	public TicTacToe()	{
		gui = new GUI(this);
	}

	public void setLetter()	{
		count++;
		/*Calculate whose turn it is*/
		if(count % 2 == 0)
			letter = "O";
		else
			letter = "X";
	}
	public void checkWin()	{
		String[] data = gui.getData();
		/*Determine who won*/
		for(int i=0; i<=7; i++){
			if(	data[winCombinations[i][0]] != "" &&
				data[winCombinations[i][0]] == data[winCombinations[i][1]] && 
				data[winCombinations[i][1]] == data[winCombinations[i][2]]){
				win = true;
			}
		}
	}
	public void printWin()	{
		/*Show a dialog when game is over*/
		if(win == true){
			gui.showMessage(letter + " wins the game!");
			if(letter.equals("X"))  xWins++;
			else                    oWins++;
			gui.playAgainDialog();
		} else if(count == 9 && win == false){
			gui.showMessage("The game was tie!");
			gui.playAgainDialog();
		}	
	}
	public void reset()	{
		win = false;
		count = 0;
		gui.reset();
	}
	public String getLetter()	{
		return letter;
	}
	public void undo()	{
		count -= 2;
		setLetter();
		gui.undo();
		gui.updateButtons();
	}
	public void redo()	{
		setLetter();
		gui.redo();
		gui.updateButtons();
	}
}
